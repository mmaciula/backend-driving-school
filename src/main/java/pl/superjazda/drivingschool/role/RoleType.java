package pl.superjazda.drivingschool.role;

public enum RoleType {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
